import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Dimensions } from "react-native";

export default function Card({ icon, date, task }) {
  return (
    <View style={styles.card}>
      <Image source={icon} style={styles.iconContainer} />
      <View style={styles.description}>
        <Text style={styles.date}>{date}</Text>
        <Text style={styles.task}>{task}</Text>
      </View>
    </View>
  );
}
const screenWidth = Math.round(Dimensions.get("window").width);
const styles = StyleSheet.create({
  card: {
    backgroundColor: "#F9F9Fb",
    height: 70,
    borderRadius: 10,
    width: screenWidth - 30,
    marginLeft: 15,
    display: "flex",
    flexDirection: "row",
    marginTop: 15,
  },
  iconContainer: {
    width: 40,
    height: 40,
    marginTop: 15,
    marginLeft: 15,
  },
  description: {
    display: "flex",
    justifyContent: "center",
    marginLeft: 15,
  },
  date: {
    fontSize: 14,
    color: "#9A9A9A",
  },
  task: {
    fontSize: 18,
    color: "#212121",
  },
});
