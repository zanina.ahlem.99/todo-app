import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import bg from "../images";

export default function Header() {
  return (
    <View style={Styles.Header}>
      <Image source={bg} />
      <Text style={Styles.title}>my todos</Text>
    </View>
  );
}

const Styles = StyleSheet.create({
  Header: {
    height: 80,
    paddingTop: 38,
    backgroundColor: "#ACD092",
  },
  title: {
    textAlign: "center",
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
});
