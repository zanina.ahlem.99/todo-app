import drinks from "./drinks.png";
import places from "./places.png";
import tv from "./tv.png";
import bag from "./bag.png";
import add from "./add.png";
import bg from "./bg.png";

export { drinks, places, tv, bag, add, bg };
