import React, { useState } from "react";
import {
  StyleSheet,
  View,
  FlatList,
  Image,
  Modal,
  TextInput,
  TouchableOpacity,
  Animated,
  Text,
  ScrollView,
  Button,
} from "react-native";
import { Header, Card } from "./components";
import { drinks, places, tv, bag, calendar, add } from "./images";
import { Dimensions } from "react-native";

export default function App() {
  const [isVisible, setIsVisible] = useState(false);
  const [icon, setIcon] = useState("");
  const [task, setTask] = useState("");
  const [date, setDate] = useState("");
  const [todos, setTodos] = useState([]);

  let fadeAnim = new Animated.Value(0);
  React.useEffect(() => {
    if (isVisible) {
      Animated.timing(fadeAnim, {
        toValue: 500,
        duration: 500,
      }).start();
    }
  }, [fadeAnim, isVisible]);
  const onPress = () => {
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 300,
    }).start(() => {});
  };
  const InterpolateColor = fadeAnim.interpolate({
    inputRange: [0, 400, 500],
    outputRange: ["transparent", "transparent", "rgba(0,0,0,0.5)"],
  });

  const screenWidth = Math.round(Dimensions.get("window").width);
  const screenHeight = Math.round(Dimensions.get("window").height);

  const styles = StyleSheet.create({
    container: {
      height: screenHeight - 80,
    },
    list: {
      paddingBottom: 105,
      height: screenHeight + 25,
    },
    add: {
      height: 75,
      width: 75,
    },
    footer: {
      position: "absolute",
      bottom: -70,
      right: 10,
    },
    overlay: {
      height: screenHeight * 0.4,
    },
    popupContent: {
      height: screenHeight * 0.6,
      backgroundColor: "#ffffff",
      borderTopLeftRadius: 15,
      borderTopRightRadius: 15,
    },
    iconContainer: {
      width: 55,
      height: 55,
    },
    scrollView: {
      width: screenWidth,
      marginRight: 20,
    },
    tastList: {
      height: 63,
      marginTop: 20,
      marginBottom: 20,
    },
    seperator: {
      width: 20,
    },
    tip: {
      height: 6,
      width: 50,
      borderRadius: 4,
      marginBottom: 10,
      marginLeft: screenWidth / 2 - 25,
      backgroundColor: "#ffffff",
    },
    input: {
      backgroundColor: "#f3f3f7",
      color: "#242427",
      padding: 15,
      fontSize: 18,
      marginLeft: 20,
      marginRight: 20,
      marginTop: 10,
      borderRadius: 10,
    },
    description: {
      fontSize: 14,
      paddingLeft: 20,
      paddingTop: 15,
      color: "#757575",
    },
    loginScreenButton: {
      marginRight: 20,
      marginLeft: 20,
      marginTop: 30,
      paddingTop: 15,
      paddingBottom: 15,
      backgroundColor: "#0a84ff",
      borderRadius: 10,
      borderWidth: 1,
      borderColor: "#fff",
    },
    loginText: {
      color: "#fff",
      fontSize: 16,
      fontWeight: "700",
      textAlign: "center",
      paddingLeft: 10,
      paddingRight: 10,
    },
    notSelected: {
      borderWidth: 2,
      borderStyle: "solid",
      borderColor: "transparent",
      padding: 2,
      borderRadius: 35,
    },
    red: {
      borderColor: "#FF5D5D80",
      borderWidth: 2,
      borderStyle: "solid",
      padding: 2,
      borderRadius: 35,
    },
    blue: {
      borderColor: "#6AA0FF80",
      borderWidth: 2,
      borderStyle: "solid",
      padding: 2,
      borderRadius: 35,
    },
    violet: {
      borderColor: "#7268FF80",
      borderWidth: 2,
      borderStyle: "solid",
      padding: 2,
      borderRadius: 35,
    },
    orange: {
      borderColor: "#FFC52780",
      borderWidth: 2,
      borderStyle: "solid",
      padding: 2,
      borderRadius: 35,
    },
  });

  const openModal = () => {
    setIsVisible(true);
  };

  const handleDone = (key) => {
    let todo = todos.filter((todo) => todo.id === key);
  };

  const onChangeTask = (text) => {
    setTask(text);
  };

  const onChangeDate = (text) => {
    setDate(text);
  };

  const addTask = () => {
    let todo = Object.assign([], todos);
    todo.push({
      id: todos + 1,
      text: task,
      date: date,
      icon: icon,
      done: false,
    });
    setTodos(todo);
    setTask("");
    setDate("");
    setIcon("");
    setIsVisible(false);
  };

  const setTaskIcon = (type) => {
    setIcon(type);
  };

  const getIcon = (icon) => {
    switch (icon) {
      case "drinks":
        return drinks;
      case "tv":
        return tv;
      case "bag":
        return bag;
      case "places":
        return places;
    }
  };

  return (
    <View style={styles.container}>
      <Header />
      <View>
        <View style={styles.list}>
          <FlatList
            data={todos}
            renderItem={({ item, index }) => (
              <TouchableOpacity onPress={() => handleDone(index)}>
                <Card
                  icon={getIcon(item.icon)}
                  date={item.date}
                  task={item.text}
                  key={index.toString}
                />
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
      <View style={styles.footer}>
        <TouchableOpacity onPress={openModal}>
          <Image source={add} style={styles.add} />
        </TouchableOpacity>
      </View>
      <Modal animationType="slide" transparent visible={isVisible}>
        <Animated.View
          style={[styles.mainContainer, { backgroundColor: InterpolateColor }]}
        >
          <TouchableOpacity activeOpacity={1} onPress={onPress}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => {
                setIsVisible(false);
              }}
            >
              <View style={styles.overlay}></View>
            </TouchableOpacity>
          </TouchableOpacity>
          <View style={styles.tip}></View>
          <View style={styles.popupContent}>
            <View style={styles.tastList}>
              <ScrollView style={styles.scrollView} horizontal={true}>
                <View style={styles.seperator}></View>
                <TouchableOpacity
                  style={icon === "drinks" ? styles.red : styles.notSelected}
                  onPress={() => setTaskIcon("drinks")}
                >
                  <Image source={drinks} style={styles.iconContainer} />
                </TouchableOpacity>
                <View style={styles.seperator}></View>
                <TouchableOpacity
                  onPress={() => setTaskIcon("bag")}
                  style={icon === "bag" ? styles.blue : styles.notSelected}
                >
                  <Image source={bag} style={styles.iconContainer} />
                </TouchableOpacity>
                <View style={styles.seperator}></View>
                <TouchableOpacity
                  onPress={() => setTaskIcon("places")}
                  style={icon === "places" ? styles.orange : styles.notSelected}
                >
                  <Image source={places} style={styles.iconContainer} />
                </TouchableOpacity>
                <View style={styles.seperator}></View>
                <TouchableOpacity
                  onPress={() => setTaskIcon("tv")}
                  style={icon === "tv" ? styles.violet : styles.notSelected}
                >
                  <Image source={tv} style={styles.iconContainer} />
                </TouchableOpacity>
                <View style={styles.seperator}></View>
              </ScrollView>
            </View>
            <Text style={styles.description}>Add task</Text>
            <TextInput
              style={styles.input}
              onChangeText={(text) => onChangeTask(text)}
              value={task}
            />
            <Text style={styles.description}>Set due date</Text>
            <TextInput
              style={styles.input}
              onChangeText={(text) => onChangeDate(text)}
              value={date}
            />
            <TouchableOpacity
              style={styles.loginScreenButton}
              onPress={() => addTask()}
              underlayColor="#fff"
            >
              <Text style={styles.loginText}>Add task</Text>
            </TouchableOpacity>
          </View>
        </Animated.View>
      </Modal>
    </View>
  );
}
